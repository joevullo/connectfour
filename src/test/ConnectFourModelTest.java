package test;

import com.company.ConnectFourModel;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

public class ConnectFourModelTest {

    ConnectFourModel testModel;

    /**
     * Tests the setup of the board by setting it up, then comparing the number
     * of elements that exist in the array and then seeing all those elements are empty or not.
     *
     * @throws Exception
     */
    @Test
    public void testGameBoardSetup() throws Exception {
        testModel = new ConnectFourModel();
        assertNotEquals(testModel.getGameBoardState() , null);

    }

    /**
     * If the previous test passed then this can be performed safely, and it is required for the next tests
     * anyway.
     */
    @Before
    public void setupModel() {
        testModel = new ConnectFourModel();
    }

    /**
     * Enters a single piece and then checks the board state to see if that position is now owned
     * by the player.
     *
     */
    @Test
    public void testEnterSinglePiece() {
        int playerID = 1;
        int enteredColumn = 1;
        testModel.enterBoardPiece(0,enteredColumn,playerID);
        int model [][] = testModel.getGameBoardState();
        int lastRowPostion = testModel.getNumberOfCols();

        assertEquals(testModel.getGameBoardState()[lastRowPostion-1][1] , playerID);
    }

//    /**
//     * Much like the previous test however this one checks the result by seeing what value the method returned.
//     */
//    @Test
//    public void anotherTestEnterSinglePiece() {
//        int playerID = 1;
//        int enteredColumn = 1;
//        int result = testModel.enterBoardPiece(0, enteredColumn, playerID);
//
//        assertEquals(testModel.getGameBoardState()[result][1] , playerID);
//    }

    /**
     * Enters pieces using the enter board piece method to create a vertical win situation and
     * then checks the model to see if the situation is a win.
     *
     */
    @Test
    public void testVerticalWin() {

        testModel.enterBoardPiece(0,0,1);
        testModel.enterBoardPiece(0,1,2);
        testModel.enterBoardPiece(0,0,1);
        testModel.enterBoardPiece(0,1,2);
        testModel.enterBoardPiece(0,0,1);
        testModel.enterBoardPiece(0,1,2);
        testModel.enterBoardPiece(0,0,1);

        assertEquals(testModel.checkWinWholeBoard() , true);
    }

    /**
     * Enters some random pieces, resets the board and then tests to see if all the elements have been cleared.
     * @throws Exception
     */
    @Test
    public void testResetBoard() throws Exception {
        boolean badResult = false;

        testModel.enterBoardPiece(0,1,2);
        testModel.enterBoardPiece(0,0,1);
        testModel.enterBoardPiece(0,1,2);
        testModel.enterBoardPiece(0,0,1);

        testModel.setBoardEmpty();

        int a [][] = testModel.getGameBoardState();
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 7; j++) {
                if (a[i][j] != 0) {
                    badResult = true;
                }
            }
        }

        assertEquals(badResult, false);
    }

    /**
     * Like the previous test but this time tests for a horizontal win situation.
     * @throws Exception
     */
    @Test
    public void testHorizontalWin() throws Exception {
        testModel.enterBoardPiece(0,0,1);
        testModel.enterBoardPiece(0,0,2);
        testModel.enterBoardPiece(0,1,1);
        testModel.enterBoardPiece(0,1,2);
        testModel.enterBoardPiece(0,2,1);
        testModel.enterBoardPiece(0,2,2);
        testModel.enterBoardPiece(0,3,1);

        assertEquals(testModel.checkWinWholeBoard() , true);
    }


    @Test
    public void testAddPlayerScore() throws Exception {

        int playerID = 1;
        int playerExpectedWins = 2;

        testModel.addPlayerWin(playerID);
        testModel.addPlayerWin(playerID);

        assertEquals(testModel.getPlayerNoWins(playerID),playerExpectedWins);

    }


}