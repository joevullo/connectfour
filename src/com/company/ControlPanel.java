package com.company;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by jvullo on 01/11/14.
 */
public class ControlPanel extends JPanel implements ActionListener {

    private final String RESET_SCORE_TEXT = "Reset Score";
    private final String END_GAME_TEXT = "End Game";
    private final String START_AI_PLAYER_TEXT = "Start AI Player";
    private final String STOP_AI_PLAYER_TEXT = "Stop AI Player";


    private final ConnectFourController controller;

    private JButton endGameButton = new JButton(END_GAME_TEXT);
    private JButton resetScoreButton = new JButton(RESET_SCORE_TEXT);
    private JButton startAIButton = new JButton(START_AI_PLAYER_TEXT);;

    public ControlPanel(ConnectFourController controller) {
        super();
        this.controller = controller;

        //setLayout(new FlowLayout());
        setLayout(new GridLayout(3,1));
        add(endGameButton);
        add(resetScoreButton);

        startAIButton.addActionListener(this);
        add(startAIButton);

        endGameButton.addActionListener(this);
        add(endGameButton);

        resetScoreButton.addActionListener(this);
        add(resetScoreButton);
    }

    @Override
    public void paintComponent(Graphics g) {

        //System.out.println(controller.getAIModeFromModel()+"");

        if (controller.getAIModeFromModel()) {
            startAIButton.setText(STOP_AI_PLAYER_TEXT);
        } else {
            startAIButton.setText(START_AI_PLAYER_TEXT);
        }

    }

    @Override
    public void actionPerformed(ActionEvent event) {
        if (event.getSource() == endGameButton)
            controller.endGame();
        else if (event.getSource() == resetScoreButton)
            controller.resetGameScores();
        else if (event.getSource() == startAIButton) {
            if (controller.getAIModeFromModel()) {
                controller.setAIPlayer(false);
            } else {
                controller.setAIPlayer(true);
            }
        }

        revalidate();
        repaint();
    }
}

