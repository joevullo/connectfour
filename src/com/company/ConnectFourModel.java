package com.company;

import java.util.Observable;
import java.util.Random;

/**
 * Created by jvullo on 30/10/14.
 */
public class ConnectFourModel extends Observable {

    //Values for the board. //This could be defined in the view perhaps?
    //Or does the winner method need rows and columns?
    //private /*@ spec_public non_null @*/ static final int NUMBER_OF_SQUARES = 49;
    //private /*@ spec_public non_null @*/ static final int ROWS = (int) Math.sqrt(NUMBER_OF_SQUARES);
    //private /*@ spec_public non_null @*/ static final int COLUMNS = ROWS; // COLUMNS are currently the same size as the ROWS.

    private /*@ spec_public non_null @*/ static final int NUMBER_OF_SQUARES = 42;
    private /*@ spec_public non_null @*/ static final int ROWS = 6;
    private /*@ spec_public non_null @*/ static final int COLUMNS = 7;

    //Ints that represent the ID's for the players.
    public  static final int EMPTY_SQUARE = 0;
    public  static final int PLAYER_1_ID = 1;
    public  static final int PLAYER_2_ID = 2;

    private /*@ spec_public non_null @*/ boolean AIMode = false;
    private /*@ spec_public non_null @*/ int currentPlayer = 1;

    private /*@ spec_public non_null @*/ int player1Wins = 0;
    private /*@ spec_public non_null @*/ int player2Wins = 0;

    private /*@ spec_public non_null @*/ int[][] gameBoard;

    //Variables that represent straight directions
    private static final int LEFT_DIR = 0;
    private static final int RIGHT_DIR = 1;
    private static final int UP_DIR = 2;
    private static final int DOWN_DIR = 3;
    //Variables that represent diagonal directions
    private static final int UP_LEFT_DIR = 4;
    private static final int UP_RIGHT_DIR = 5;
    private static final int DOWN_LEFT_DIR = 6;
    private static final int DOWN_RIGHT_DIR = 7;
    //Array of the directions, so I can for loop later.
    private static final int [] DIRECTIONS = {LEFT_DIR, RIGHT_DIR, UP_DIR, DOWN_DIR,
            UP_LEFT_DIR, UP_RIGHT_DIR , DOWN_LEFT_DIR, DOWN_RIGHT_DIR};

    /**
     * Sets up, the gameboard initially the board to be empty..
     */
    /*@ public normal_behavior
        ensures (gameBoard.length != 0);
    @*/
    public ConnectFourModel() {
        gameBoard = new int[ROWS][COLUMNS];

        if (gameBoard != null) {
            setBoardEmpty();
        }
    }

    /**
     * Returns the gameboard state, the controller references this and the view uses the controller to gain access to it.
     */
    public /*@ pure @*/int[][] getGameBoardState() {
        return gameBoard;
    }

    /**
     * Evaluates whether the move is a valid move or not.
     *
     * @param selectedRow
     * @param selectedColumn
     * @param player
     */
    public boolean isMoveValid(int selectedRow, int selectedColumn, int player) {

        //Functionality has been removed, but I have the parameter in the method
        //Just in case I want to restore this.
        if (selectedRow != 0) {
            //return false;
        }

        //for the cols of the column.
        for (int j = ROWS - 1; j > -1; j--) {
            //If the column has an empty piece, then return true.
            if (gameBoard[j][selectedColumn] == EMPTY_SQUARE) {
                return true;
            }
        }
        return false;
    }

    /**
     * Uses the column passed in to enter a new piece. Originally this method was constrained so that
     *
     * @param selectedColumn - The column which we want to add a piece to.
     */
    /*@ public normal_behavior
        requires (selectedRow >= 0 && selectedColumn>= 0 && gameBoard != null);
        ensures (gameBoard != \old(gameBoard) || \result == -1);
    @*/
    public void enterBoardPiece(int selectedRow, int selectedColumn, int player) {

       //Not using selectedRow anymore, could remove it.

        //for the cols of the column.
        for (int j = ROWS - 1; j > -1; j--) {
            //if the game board column has an empty item, set the owner to the new player.
            if (gameBoard[j][selectedColumn] == EMPTY_SQUARE) {
                gameBoard[j][selectedColumn] = player;
                break;
            }
        }

        setChanged();
        notifyObservers();
    }

    /**
     * Gets the number of squares,
     *
     * @return Returns the number of squares.
     */
    public int getTotalNumberOfElements() {
        return NUMBER_OF_SQUARES;
    }

    /**
     * Gets the number of rows.
     * @return Returns the number of squares.
     */
    public int getNumberOfRows() {
        return ROWS;
    }

    /**
     * Gets the number of columns,
     * @return Returns the number of squares.
     */
    public int getNumberOfCols() {
        return COLUMNS;
    }

    /**
     * Returns the current player.
     */
    public int getCurrentPlayer() {
        return currentPlayer;
    }

    /**
     * Simple getter for the AIMode value.
     *
     * @return
     */
    public boolean getAIMode() {
        return AIMode;
    }

    /**
     * Simple setter for the AI Mode.
     * @param value - New Value on the AI Mode.
     */
    public void setAIMode (boolean value) {
        AIMode = value;
    }

    /**
     * Sets the player making the move to be the next one.
     * Used by the controller after calling enter piece so that the
     * next piece entered will belong the following player.
     */
    /*@ public normal_behavior
    requires (currentPlayer == 1 || currentPlayer == 2);
    ensures (currentPlayer != \old(currentPlayer));
    @*/
    public void setNextCurrentPlayer() {
        //Set the next piece to be entered by the next player.
        if (currentPlayer == 1) {
            currentPlayer = 2;
        } else if (currentPlayer == 2) {
            currentPlayer = 1;
        }

        setChanged();
        notifyObservers();
    }

    public boolean checkWinWholeBoard() {
        if (checkWinWholeBoardByPlayer(PLAYER_1_ID) || checkWinWholeBoardByPlayer(PLAYER_2_ID)) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * Checks the whole board for a winning solution. Originally I Planned to optimise this by using the least amount of for loops possible
     * however going diagonally proved to be trickier than I suspected. It has been programmed and left for now but I plan to optimise the solution
     * in the next assignment.
     *
     * NOTE - I have not optimised this because Ian said I would recieve no extra marks for doing so and I have other assignements to complete soon. 
     *
     * @return True if a winning solution is found, false if not.
     */
    /*@ public normal_behavior
    requires (getCurrentPlayer() == 1 || getCurrentPlayer() == 2);
    ensures (gameBoard == \old(gameBoard));
    @*/
    public boolean checkWinWholeBoardByPlayer(int playerID) {
        //int playerID = getCurrentPlayer();

        //Uses these variables to count the number of common elements in a row.
        int honPieceCount = 0;
        int vertPieceCount = 0;

        int leftToRightDiaPieceCount = 0;
        int rightToLeftDiaPieceCount = 0;

        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLUMNS; j++) {

                //Looking for horizontal pieces.
                if (gameBoard[i][j] == playerID) {
                    honPieceCount++;
                } else {
                    //If concurrent piece is found then set the value back to 0.
                    honPieceCount = 0;
                }

                //Looking for vertical pieces
                //j & i need to be restrained to rows or columns because I am causally swapping them here.
                if (i < COLUMNS && j < ROWS) {
                    if (gameBoard[j][i] == playerID) {
                        vertPieceCount++;
                    } else {
                        //If concurrent piece is found then set the value back to 0.
                        vertPieceCount = 0;
                    }
                }

                //Compares the number of pieces and returns true if more than four for either horizontal or diagonal.
                if (honPieceCount >= 4 || vertPieceCount >= 4) {
                    return true;
                }
            }
        }

        //The following code needs refactoring.

        //Here I have had to compute the top half the of the board only because of the direction that I want to go in.
        //I am sure there is a solution to get this into one for loop but I am.
        for( int r = COLUMNS * 2; r >= 0; r-- ) {
            //Keeps looping while c is less than or equal to r.
            for (int c = 0; c < COLUMNS; c++) {
                int i = c + r;
                //int i = r + c;
                //Constrains the values because I've double the length of the for loop.
                if (i < ROWS && c < ROWS) {
                    //System.out.print("("+i+","+c+")");

                    if (gameBoard[i][c] == playerID) {
                        leftToRightDiaPieceCount++;
                    } else {
                        leftToRightDiaPieceCount = 0;
                    }

                    if (leftToRightDiaPieceCount >= 4 || leftToRightDiaPieceCount >= 4) {
                        return true;
                    }
                }
            }
            leftToRightDiaPieceCount = 0;
        }


        //Here is the bottom half of the board from the right to left direction.
        for( int r = 0; r < COLUMNS * 2; r++ ) {
            //Keeps looping while c is less than or equal to r.
            for (int c = 0; c < COLUMNS; c++) {
                int i = c + r;
                //int i = r + c;
                //Constrains the values because I've double the length of the for loop.
                if (i < ROWS && c < ROWS) {
                    //System.out.print("("+c+","+i+")");

                    if (gameBoard[i][c] == playerID) {
                        leftToRightDiaPieceCount++;
                    } else {
                        leftToRightDiaPieceCount = 0;
                    }

                    if (leftToRightDiaPieceCount >= 4 || leftToRightDiaPieceCount >= 4) {
                        return true;
                    }
                }
            }
            leftToRightDiaPieceCount = 0;
        }


        //Evaluates diagonally from right to left.
        //Again I'm sure this can be refactored into one for loop for both directions.
        for( int r = ROWS * 2 ; r >= 0; r-- ) {
            //Keeps looping while c is less than or equal to r.
            for( int c = 0 ; c <= r; c++ ) {
                int i = r - c;
                //Constrains the values because I've double the length of the for loop.
                if( i < ROWS && c < ROWS ) {
                    //Looking for horizontal pieces.
                    if (gameBoard[i][c] == playerID) {
                        rightToLeftDiaPieceCount++;
                    } else {
                        rightToLeftDiaPieceCount = 0;
                    }

                    if (rightToLeftDiaPieceCount >= 4 || rightToLeftDiaPieceCount >= 4) {
                        return true;
                    }

                }
            }
            rightToLeftDiaPieceCount = 0; //Having it set back to 0 here stops the possibility of it wrapping around in the corners and give a false win.
        }


        return false;
    }

    /**
     * Sets all the board values with the "empty" value i.e 0.
     * Used when first setting up the board and to reset the view
     * at the end of a game.
     */
    /*@ public normal_behavior
    requires (gameBoard != null);
    ensures (\forall int i; i < ROWS; (\forall int j; j<COLUMNS; gameBoard[i][j]==0));
    @*/
    public void setBoardEmpty() {
        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLUMNS; j++) {
                gameBoard[i][j] = EMPTY_SQUARE;
            }
        }

        setChanged();
        notifyObservers();
    }


    /**
     * Checks if the board is full, then a win condition.
     */
    public boolean checkForADrawCondition() {
        //Assumed to be true until proven guilty.
        boolean boardFilled = true;

        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLUMNS; j++) {
                //If any of the values of the board are equal to empty, then the
                //The board is not filled.
                if(gameBoard[i][j] == EMPTY_SQUARE) {
                   //System.out.println("" +i + "," + j + " is true");
                    boardFilled = false;
                    break;
                }
            }
        }

        //If the board is filled
        //System.out.println("Draw run");
        if (boardFilled) {
            //And there is no winning condition.
            if(!checkWinWholeBoard()) {
                //Then we return true because the game is a draw.
                return true;
            }
        }

        //Otherwise just return false.
        //System.out.println("False");
        return boardFilled;
    }

    /**
     * Used to get a players wins, enter the parameter
     *
     * @param player - The player ID.
     *
     * @return Returns a players wins.
    */
    /*@ public normal_behavior
    requires (player == PLAYER_1_ID || player == PLAYER_2_ID);
    @*/
    public int getPlayerNoWins(int player) {

        if (player == PLAYER_1_ID) {
            return player1Wins;
        } else if (player == PLAYER_2_ID) {
            return player2Wins;
        } else {
            return -1;
        }
    }

    /**
     * Adds one to the number of player wins, using the parameter passed in to select the player.
     * @param player - The player who has won.
     */
    /*@ public normal_behavior
    requires (player == PLAYER_1_ID || player == PLAYER_2_ID);
    requires (player1Wins >= 0 && player2Wins >= 0);
    ensures (player1Wins != \old(player1Wins) || player2Wins != \old(player2Wins));
    @*/
    public void addPlayerWin (int player) {
        if (player == PLAYER_1_ID) {
            player1Wins++;
        } else if (player == PLAYER_2_ID) {
            player2Wins++;
        }

        setChanged();
        notifyObservers();
    }

    /**
     * Resets the players scores back to 0.
     * Uses for loops and array cols just in case the number of players changes in the future.
     */
    /*@ public normal_behavior
    ensures (player1Wins == 0 && player2Wins == 0);
    @*/
    public void resetScores() {
        player1Wins = 0;
        player2Wins = 0;
        setChanged();
        notifyObservers();
    }

    /**
     * Gets the current board state, and makes a clever move
     * based on....
     */
    public void AIPlayerMove(int player) {
        Random r = new Random();
        int randomInt = r.nextInt(COLUMNS-0) + 0;
        //enterPiece(randomInt, player);
        //printGameState();
    }

    /**
     * Performs a random move for the AI Player.
     */
    public void AIPlayerRandomMove(int player) {
        Random r = new Random();
        int randomInt = r.nextInt(COLUMNS-0) + 0;
        //addNewPiece(randomInt, player);

        //Danger here of an infinite loop.
        if (isMoveValid(0,randomInt,player)) {
            //If its a valid move then make it.
            enterBoardPiece(0, randomInt, player);
        } else {
            // if the move isn't valid, check if the board is drawn.
            if (checkForADrawCondition()) {
                // If there is a draw condition then finish.
                return;
            } else {
                //otherwise just try entering a piece again.
                AIPlayerRandomMove(player);
            }
        }
    }

    /**
     * Used for testing, prints the state of the board so that you can see where the array
     * has stored its pieces.
     */
    public void printGameState() {

        System.out.println("");

        for (int i = 0; i < ROWS; i++) {
            System.out.print("Row = " +i + " ");
            for (int j = 0; j < COLUMNS; j++) {
                System.out.print(gameBoard[i][j]);
            }
            System.out.println("");
        }
    }

    /**
     * Not decided what this is doing yet, or how its doing it.
     */
    public void runArtificialComputer() {
        AIBetterMove(getCurrentPlayer());
    }

    public int getPieceValueByDirection(int rowPos, int colPos, int player, int direct) {

        //TODO Could eval by player number, do for both players at once like min max.

        int numberOfPieceCount = 0;

        int rowModifier = 0;
        int colModifier = 0;

        //Switch to find out what direction we are going in
        // and set the modifiers produce that effect.
        switch (direct) {
            case LEFT_DIR:
                rowModifier = -1;
                colModifier = 0;
                break;
            case RIGHT_DIR:
                rowModifier = 1;
                colModifier = 0;
                break;
            case UP_DIR:
                rowModifier = 0;
                colModifier = -1;
                break;
            case DOWN_DIR:
                rowModifier = 0;
                colModifier = +1;
                break;
            case UP_LEFT_DIR:
                rowModifier = -1;
                colModifier = -1;
                break;
            case UP_RIGHT_DIR:
                rowModifier = 1;
                colModifier = -1;
                break;
            case DOWN_LEFT_DIR:
                rowModifier = -1;
                colModifier = 1;
                break;
            case DOWN_RIGHT_DIR:
                rowModifier = 1;
                colModifier = 1;
                break;
        }

        int count = 0;
        int tempRow = rowPos;
        int tempCol = colPos;

        while (count < 4) {
            tempRow += rowModifier;
            tempCol += colModifier;
            //If the position has gone beyond the bounds of the board.
            if (tempRow < ROWS && tempRow >= 0 && tempCol < COLUMNS && tempCol >= 0) {
                //This ensures that the pieces are counted concurrently.
                if (gameBoard[tempRow][tempCol] == player) {
                    numberOfPieceCount++;
                } else {
                    //Essentially, stop counting the first time the piece doesn't equal the users.
                    break;
                }
            } else {
                //Break if the bounds have been exceeded.
                break;
            }
            count++;
        }

        return numberOfPieceCount;
    }

    public int getPieceValueInAllDirections(int i, int j, int player) {

        int overallPieceRating = 0;

        //Before do anything we should check whether the piece we are sitting on is not player owned.
        //If it is not player owned then it has a strategic value of nothing, because it can't be used.
        //Be careful, this assumes the col & row are valid.
        if (gameBoard[i][j] != EMPTY_SQUARE) {
            return 0;
        }

        //This for loop goes through and asks for the direction values for all the directions.
        for (int c = 0; c < DIRECTIONS.length; c++) {
            overallPieceRating += getPieceValueByDirection(i, j, player, DIRECTIONS[c]);
        }

        return overallPieceRating;
    }


    public void AIBetterMove(int playerID) {

        int highestPieceValue = 0;
        int bestRowPostion = 0;
        int bestColPostion = 0;

        int newPiecevalue = 0;

        //For all the positions on the board.
        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLUMNS; j++) {
                newPiecevalue = getPieceValueInAllDirections(i, j, playerID);
                //System.out.print(a+" "); //
                if (newPiecevalue > highestPieceValue) {
                    highestPieceValue = newPiecevalue;
                    bestRowPostion = i;
                    bestColPostion = j;
                }
            }
            //System.out.println();

        }
        //System.out.println("----------------------------");
        //System.out.println("Chosen Position = " + bestRowPostion + " , " + bestColPostion); //TODO. remove
        //System.out.println();

        //If the highest of the whole board is 0, then we should just make a random move.
        if (highestPieceValue != 0) {
            enterBoardPiece(bestRowPostion, bestColPostion, playerID);
        } else {
            AIPlayerRandomMove(playerID);
        }
    }
}