package com.company;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

import static com.company.ConnectFourModel.PLAYER_1_ID;
import static com.company.ConnectFourModel.PLAYER_2_ID;
import static com.company.ConnectFourView.*;


/**
 *
 * This creates a view
 *
 * Created by jvullo on 01/11/14.
 */
public class ScorePanel extends JPanel {

    private final ConnectFourController controller;

    private JTextField player1WinField = new JTextField(1);
    private JTextField player2WinField = new JTextField(1);

    private JLabel player1Label = new JLabel("Player 1: ");
    private JLabel player2Label = new JLabel("Player 2: ");

    private JLabel scorePanelTitle = new JLabel("Player Scores");

    public ScorePanel(ConnectFourController controller) {
        super();
        this.controller = controller;

        //setLayout(new GridLayout(3,3));
        setLayout(new FlowLayout(FlowLayout.CENTER, 5 , GAME_BOARD_SIZE/20));
        setBorder(new EmptyBorder(10,10,10,10));

        //Configure the fields.
        player1WinField.setEditable(false);
        player2WinField.setEditable(false);

        //Setting the fields to be centered.
        player1Label.setHorizontalAlignment(SwingConstants.CENTER);
        player2Label.setHorizontalAlignment(SwingConstants.CENTER);
        player1WinField.setHorizontalAlignment(SwingConstants.CENTER);
        player2WinField.setHorizontalAlignment(SwingConstants.CENTER);
        scorePanelTitle.setHorizontalAlignment(SwingConstants.CENTER);

        //Sets the colours for the players.
        player1Label.setForeground(PLAYER_COLOR_1);
        player2Label.setForeground(PLAYER_COLOR_2);

        //Adding all the items in thr right order to be show in the view.
        add(scorePanelTitle);
        add(player1Label);
        add(player1WinField);
        add(player2Label);
        add(player2WinField);
    }

    public void paintComponent(Graphics g) {

        //Sets the ID's in the fields.
        player1WinField.setText(""+controller.getPlayerWins(PLAYER_1_ID));
        player2WinField.setText(""+controller.getPlayerWins(PLAYER_2_ID));

        //Sets the current player to be bold.
        Font font = player1Label.getFont();
        Font boldFont = new Font(font.getFontName(), Font.BOLD, font.getSize());
        if (controller.getCurrentPlayerFromModel() == PLAYER_1_ID) {
            player1Label.setForeground(PLAYER_COLOR_1);
            player2Label.setForeground(Color.black);
            //player1Label.setForeground(Color.black);
        } else if (controller.getCurrentPlayerFromModel() == PLAYER_2_ID) {
            player2Label.setForeground(PLAYER_COLOR_2);
            player1Label.setForeground(Color.black);
        }
    }

    public void setEnableChange(boolean enable) {
        //changeButton.setEnabled(enable);
    }

    public Dimension getPreferredSize() {
        return new Dimension(50,50);
    }

    public Dimension getMinimumSize() {
        return getPreferredSize();
    }

    public Dimension getMaximumSize() {
        return getPreferredSize();
    }

}
