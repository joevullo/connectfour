package com.company;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * Implements an action listener which stores the position of the grid that the action listener is used upon.
 *
 * Created by jvullo on 01/11/14.
 */
public class MyActionListener implements ActionListener {

    private int row = 0;
    private int col = 0;

    ConnectFourController controller;

    /**
     * Constructor that stores the values of the row and column of the piece this listener belongs to.
     * Also stores the controller so that the actionPerformed method can callback to the controller and
     * tell it to add a new piece using the column in the game.
     *
     * @param row
     * @param col
     * @param controller
     */
    public MyActionListener(int row, int col, ConnectFourController controller)
    {
        this.controller = controller;
        this.row = row;
        this.col = col;
    }

    /**
     * Callback for a method having been performed, only runs the method if the first row
     * was clicked.
     *
     * @param actionEvent
     */
    public void actionPerformed(ActionEvent actionEvent)
    {
        controller.addNewPiece(row, col);
    }
}
