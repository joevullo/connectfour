package com.company;

/**
 * Created by jvullo on 30/10/14.
 */

public class ConnectFourController {

    private final ConnectFourView view;
    private final ConnectFourModel model;

    /**
     * Pass in the the models if you want to construct them yourself with the main.
     */
    public ConnectFourController(ConnectFourView view, ConnectFourModel model) {
        this.view = view;
        this.model = model;
    }

    /**
     * This constructor makes its own view and model.
     */
    public ConnectFourController() {
        this.model = new ConnectFourModel();
        this.view = new ConnectFourView(model, this);
    }

    /**
     * Gets the stat of the board from the model.
     *
     * @return the boards values.
     */
    public int[][] getBoardState() {
        return model.getGameBoardState();
    }

    /**
     * Simple calls the model and gets the number of player wins based on the parameter passed.
     * @param player - The player that you want to get the score for.
     */
    public int getPlayerWins(int player) {
        return model.getPlayerNoWins(player);
    }

    /**
     * Starts the game again by emptying the board.
     */
    public void endGame() {
        model.setBoardEmpty();
        //Running the next current player means that after a game the players will alternate.
        model.setNextCurrentPlayer();
    }

    /**
     * Calls back to model, and sets the board back to empty.
     */
    public void resetGameScores() {
        model.resetScores();
    }

    /**
     * A wrapper for the model to add a new piece. Requires the column of which to add a new piece and
     * the player that the piece belongs to. This method allows you to override what player the piece
     * belongs to.
     *
     * @param column - The column to add a number to.
     * @param player - The player that the piece belongs to.
     * @param row -
     */
    public void addNewPiece(int row, int column, int player) {
        model.enterBoardPiece(row, column, player);
    }

    /**
     * A wrapper for the model to add a new piece by calling the model and providing it with the column to add a piece.
     * This method relies on the model to provide which player is entering.
     *
     * @param row -
     * @param column - The column to add a number to.
     */
    public void addNewPiece(int row, int column) {

        //Player mode.
        //Evaluates the move is valid and then enters a piece.
        if (model.isMoveValid(row, column, model.getCurrentPlayer())) {
            model.enterBoardPiece(row, column, model.getCurrentPlayer());
        } else {
            return;
        }

        //Returns true, if the board has a winning condition.
        if (model.checkWinWholeBoard()) {
            winGame(); //Win game will decide if it needs to set the next current player.
        } else {
            //Else just set the next player and continue the game.
            model.setNextCurrentPlayer();
        }

        // If the AI is running...
        if (model.getAIMode()) {
            model.runArtificialComputer();

            //Because the AI has moved we need to make sure the AI hasn't made a winning move.
            if (model.checkWinWholeBoard()) {
                winGame(); //Win game will decide if it needs to set the next current player.
            }
            model.setNextCurrentPlayer();
        }

        //We can be lazy and check for a draw condition last, since the method won't allow pieces to be added
        //if the board is full anyway. 
        if (model.checkForADrawCondition()) {
            view.displayDrawGame();
        }
    }

    /**
     * Simply updates the scores for the player and then calls back to view to display a dialogue
     * that asks the user if they want to play again.
     */
    private void winGame() {
        model.addPlayerWin(model.getCurrentPlayer());
        view.displayWinGame();
    }

    public void setAIPlayer(boolean val) {
        model.setAIMode(val);
    }

    /**
     * A simply getter that retrieves the current player by requesting it from the model.
     */
    public int getCurrentPlayerFromModel() {
        return model.getCurrentPlayer();
    }


    /**
     * Simple getter that returns the total number of elements the model is using.
     *
     * @return a boolean.
     */
    public int getNoOfBoardElementsFromModel() {
        return model.getTotalNumberOfElements();
    }

    /**
     * Simple getter that returns the number of columns the model is using.
     *
     * @return a boolean.
     */
    public int getNoOfColumnsFromModel() {
        return model.getNumberOfCols();
    }

    /**
     * Simple getter that returns the number of rows the model is using.
     *
     * @return a boolean.
     */
    public int getNoOfRowsFromModel() {
        return model.getNumberOfRows();
    }

    /**
     * Simple getter that asks the model if the AI player is activity.
     *
     * @return a boolean.
     */
    public boolean getAIModeFromModel() {
        return model.getAIMode();
    }

}
