package com.company;

import javax.swing.*;
import java.awt.*;
import java.util.Observer;


/**
 *
 * Holds the logic implementing the view for the program.
 * Also uses the controller to send back user feedback on the view i.e, the adding of new pieces.
 *
 * Created by jvullo on 30/10/14.
 */
public class ConnectFourView implements Observer {


    //Colors for the views, these are referred to by the gameboard, piecs and score panel classes.
    final static public Color DEFAULT_PIECE_COLOR = Color.white;
    final static public Color PLAYER_COLOR_1 = Color.cyan;
    final static public Color PLAYER_COLOR_2 = Color.red;
    final static public Color BOARD_COLOR = Color.darkGray;

    //Various views are using this to size themselves.
    public static final int GAME_BOARD_SIZE = 400;

    final private String appTitle = "Connect Four"; //Or Four Play?
    private final ConnectFourController controller;

    private ConnectFourModel model;

    private JFrame frame;
    private ScorePanel scorePane;
    private GameBoard gamePane;
    private ControlPanel controlPane;

    /**
     * This initialises the model and controller variables it also adds this
     * class to the observer
     *
     * @param model
     * @param controller
     */
    public ConnectFourView(ConnectFourModel model, ConnectFourController controller)  {
        this.model = model;
        this.controller = controller;
        model.addObserver(this);
        createView();
        update(model, null);
    }

    /**
     * Creates the view and adds a gameBoard, ScorePanel and Control Panel classes to
     * it to create the view of the game.
     */
    public void createView() {
        frame = new JFrame(appTitle);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Container contentPane = frame.getContentPane();
        contentPane.setLayout(new BorderLayout());

        gamePane = new GameBoard(controller);
        scorePane = new ScorePanel(controller);
        controlPane = new ControlPanel(controller);

        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout(GAME_BOARD_SIZE/10,GAME_BOARD_SIZE/10));

        panel.add(scorePane, BorderLayout.CENTER);
        panel.add(controlPane, BorderLayout.SOUTH);

        contentPane.add(gamePane, BorderLayout.WEST);
        contentPane.add(panel, BorderLayout.EAST);

        frame.pack();
        frame.setVisible(true);
    }

    /**
     * Creates the board and panels but with a vertical layout.
     * This was the original method used to create the view but have since moved on.
     */
    public void createGameVertical() {
        frame = new JFrame(appTitle);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Container contentPane = frame.getContentPane();
        contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));

        scorePane = new ScorePanel(controller);
        gamePane = new GameBoard(controller);
        controlPane = new ControlPanel(controller);

        contentPane.add(gamePane);
        contentPane.add(scorePane);
        contentPane.add(controlPane);

        frame.pack();
        frame.setVisible(true);
    }

    /**
     * Gets called when the model has notified the observers that some changes have been made.
     * This currently lets the game board know that some of its data needs to be corrected
     * by using update board method which belongs to GameBoard class.
     *
     * @param o
     * @param arg
     */
    public void update(java.util.Observable o, Object arg) {
        gamePane.updateBoard();
        frame.repaint();
    }


    public void displayDrawGame() {
        int dialogResult = JOptionPane.showConfirmDialog (null, "A draw has been reached " +
                ", Would you like to play again?" ,"Player Draw!",JOptionPane.YES_NO_OPTION);

        //User select yes
        if (dialogResult == 0) {
            controller.endGame();
        } else {
            //User selected no.
            System.exit(0);
        }

    }

    public void displayWinGame() {
        int dialogResult = JOptionPane.showConfirmDialog (null, "Well done player " + controller.getCurrentPlayerFromModel() +
                ", Would you like to play again?" ,"Player " + controller.getCurrentPlayerFromModel() + " is the winner!",JOptionPane.YES_NO_OPTION);

        //User select yes
        if (dialogResult == 0) {
            controller.endGame();
        } else {
            //User selected no.
            System.exit(0);
        }

    }
}
