package com.company;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

import static com.company.ConnectFourModel.*;
import static com.company.ConnectFourView.*;

/**
 * Implements the game board and the listener that allows users to enter new pieces into the game.
 *
 * Created by jvullo on 01/11/14.
 */
public class GameBoard extends JPanel {

    private final ConnectFourController controller;
    RoundPiece[][] grid; //names the grid of buttons

    private int[][] boardState;
    int rows;
    int cols;

    private MyActionListener[][] buttonListener;


    public GameBoard(ConnectFourController controller) {
        super();
        this.controller = controller;

        rows = controller.getNoOfRowsFromModel();
        cols = controller.getNoOfColumnsFromModel();

        buttonListener = new MyActionListener[rows][cols];
        //Get the board state so we can find out the amount of elements in the area.
        boardState = controller.getBoardState();

        setBackground(BOARD_COLOR);
        //setBorder(new LineBorder(Color.DARK_GRAY, 10));
        setBorder(new EmptyBorder(20,20,20,20));
        setLayout(new GridLayout(rows, cols));

        grid = new RoundPiece[rows][cols];

        for(int y = 0; y < rows; y++){
            for(int x = 0; x < cols; x++){
                grid[y][x] = new RoundPiece(0);
                buttonListener[y][x] = new MyActionListener(y,x, controller);
                grid[y][x].addActionListener(buttonListener[y][x]);
                add(grid[y][x]);
            }
        }

    }

    /**
     * When called will compare the games logical state and set the colors of the pieces
     * in the view to reflect that state.
     */
    public void updateBoard() {

        boardState = controller.getBoardState(); //Called here to ensure its up to date.

        for(int y = 0; y < rows; y++){
            for(int x = 0; x < cols; x++){

                if (boardState[y][x] == PLAYER_1_ID) {
                    grid[y][x].setColor(PLAYER_COLOR_1);
                    grid[y][x].setPlayerOwner(PLAYER_1_ID);
                } else if (boardState[y][x] == PLAYER_2_ID) {
                    grid[y][x].setColor(PLAYER_COLOR_2);
                    grid[y][x].setPlayerOwner(PLAYER_2_ID);
                } else {
                    grid[y][x].setColor(DEFAULT_PIECE_COLOR);
                    grid[y][x].setPlayerOwner(EMPTY_SQUARE);
                }
            }
        }

        revalidate();
        repaint();
    }

    public Dimension getPreferredSize() {
        return new Dimension(GAME_BOARD_SIZE,GAME_BOARD_SIZE);
    }

    public Dimension getMinimumSize() {
        return getPreferredSize();
    }

    public Dimension getMaximumSize() {
        return getPreferredSize();
    }

}


