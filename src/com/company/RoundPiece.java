package com.company;


import javax.swing.*;
import java.awt.*;

import static com.company.ConnectFourModel.*;
import static com.company.ConnectFourView.*;

/**
 * Created by jvullo on 01/11/14.
 */
public class RoundPiece extends JButton {


    Color pieceColor = DEFAULT_PIECE_COLOR;
    int playerOwner = EMPTY_SQUARE;

    public RoundPiece(int nPlayerOwner) {
        super();
        this.playerOwner = nPlayerOwner;
    }

    public void paintComponent(Graphics g) {
        int width = getWidth();
        int height = getHeight();

        if (playerOwner == PLAYER_1_ID) {
            g.setColor(PLAYER_COLOR_1);
        } else if (playerOwner == PLAYER_2_ID) {
            g.setColor(PLAYER_COLOR_2);
        } else {
            g.setColor(DEFAULT_PIECE_COLOR);
        }

        g.fillOval(0, 0, width - (width / 10), height - (height / 10));
    }

    void setColor(Color newColor) {
        pieceColor = newColor;
    }

    void setPlayerOwner(int newPlayerOwner) {
        playerOwner = newPlayerOwner;
    }

    public void paint(Graphics g) {
        paintComponent(g);
    }
}
